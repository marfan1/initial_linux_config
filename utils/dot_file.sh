#!/bin/bash

function dot_file() {
  if [ -f $HOME/.fan_dot_files/.bash_profile ]; then
    # printf "$HOME/.fan_dot_files/.bash_profile"
    dot_file="$HOME/.fan_dot_files/.bash_profile";
  elif [ -f $HOME/.bash_profile ]; then
    # printf "$HOME/.bash_profile"
    dot_file="$HOME/.bash_profile";
  elif [[ -f $HOME/.profile ]]; then
    # printf "$HOME/.profile"
    dot_file="$HOME/.profile";
  fi
  echo "$dot_file";
}

echo $(dot_file returned_dot_file_path)
