#!/bin/bash

if (( $# < 1 ))
then
  log_warn "ppa is required"
  log_warn "Usage: ./add_ppa.sh ppa"
  log_warn "Example: ./add_ppa.sh nginx/stable"
  exit 1
fi

for i in "$@"; do
  grep -h "^deb.*$i" /etc/apt/sources.list.d/* > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
    log_info "Adding ppa:$i"
    sudo add-apt-repository -y ppa:$i
  else
    log_warn "ppa:$i already exists"
  fi
done
