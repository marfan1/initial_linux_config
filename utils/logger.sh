#!/bin/bash

if [ -f "bash_files/.bash_colors" ]; then
  source bash_files/.bash_colors
fi

export VISUAL_DIVISOR="${txtgrn}===============================================================================\n${txtrst}"

function print_installation_visual_header() {
    printf $VISUAL_DIVISOR;
    printf "${txtgrngsh}$1${txtrst}\n";
    printf $VISUAL_DIVISOR;
}

function print_installation_visual_footer() {
    printf $VISUAL_DIVISOR;
}


function log_info() {
    printf "${txtblu}[$(date -u)] - $1${txtrst}\n";
}
function log_warn() {
    printf "${txtylw}[$(date -u)] - $1${txtrst}\n";
}
function log_error() {
    printf "${txtred}[$(date -u)] - $1${txtrst}\n";
}


export -f print_installation_visual_header
export -f print_installation_visual_footer
export -f log_info
export -f log_warn
export -f log_error
