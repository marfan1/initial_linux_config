#!/bin/bash

print_installation_visual_header "NGINX INSTALLATION"

# This PPA is maintained by volunteers and is not distributed by nginx.org. 
# It has some additional compiled-in modules and may be more fitting for your 
# environment.

# You can get the latest stable version of NGINX from the NGINX PPA on 
# Launchpad: You will need to have root privileges to perform the following commands.

# For Ubuntu 10.04 and newer:

# sudo -s
nginx=stable # use nginx=development for latest development version
# add-apt-repository ppa:nginx/$nginx
./utils/add_ppa.sh nginx/$nginx

sudo apt-get update
sudo apt-get install nginx


# If you get an error about add-apt-repository not existing, you will want to 
# install python-software-properties. For other Debian/Ubuntu based
# distributions, you can try the lucid variant of the PPA which is the most 
# likely to work on older package sets:

# sudo -s
# nginx=stable # use nginx=development for latest development version
# echo "deb http://ppa.launchpad.net/nginx/$nginx/ubuntu lucid main" > /etc/apt/sources.list.d/nginx-$nginx-lucid.list
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C
# apt-get update
# apt-get install nginx

print_installation_visual_footer

