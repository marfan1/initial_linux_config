#!/bin/bash

print_installation_visual_header "UMAKE INSTALLATION"

./utils/add_ppa.sh ubuntu-desktop/ubuntu-make

sudo apt-get update 

# Workaround for: http://www.discoverbits.in/139/subprocess-installed-installation-script-returned-status
sudo apt-get install python3-argcomplete=0.8.1-1ubuntu2


sudo apt-get install ubuntu-make

print_installation_visual_footer

