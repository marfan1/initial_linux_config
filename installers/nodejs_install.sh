#!/bin/bash

# https://docs.npmjs.com/getting-started/installing-node
print_installation_visual_header "NODEJS INSTALLATION"


# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install nodejs

# Node comes with npm installed so you should have a version of npm. However, 
# npm gets updated more frequently than Node does, so you'll want to make sure
# it's the latest version.
log_info "Updating npm"
sudo apt-get install npm
sudo npm install npm@latest -g

ln -s /usr/bin/nodejs /usr/bin/node

# # Using Debian, as root
# curl -sL https://deb.nodesource.com/setup_6.x | bash -
# apt-get install -y nodejs

print_installation_visual_footer

