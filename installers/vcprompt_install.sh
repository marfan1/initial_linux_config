#!/bin/bash

print_installation_visual_header "VCPROMPT INSTALLATION" 

if [ ! -d "$HOME/bin" ]; then
  mkdir $HOME/bin
fi
sudo curl -sL https://github.com/djl/vcprompt/raw/master/bin/vcprompt > $HOME/bin/vcprompt
sudo chmod 755 $HOME/bin/vcprompt

print_installation_visual_footer

