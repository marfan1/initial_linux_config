#!/bin/bash

# https://bitbucket.org/ged/ruby-pg/wiki/Home
print_installation_visual_header "libpq-dev build-essential (required by gem 'pg') INSTALLATION"

# ==============================================================================
# Before use this script choose (uncomment) a database client for install 
# ==============================================================================
  # echo "Installing Mysql Client"
  # sudo apt-get install mysql-client libmysqlclient-dev -y

  # echo "Installing postgresql"
  # sudo touch /etc/apt/sources.list.d/pgdg.list
  # echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
  # wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  #   sudo apt-key add -
  # sudo apt-get update
  # sudo apt-get install postgresql-9.6
sudo apt-get install libpq-dev build-essential

print_installation_visual_footer
