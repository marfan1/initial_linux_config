#!/bin/bash

print_installation_visual_header "RVM INSTALLATION"


log_info "Installing RVM and Rails"
  curl -#LO https://rvm.io/mpapis.asc
  gpg --import mpapis.asc
  \curl -sSL https://get.rvm.io | bash -s stable

  # gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
  # \curl -sSL https://get.rvm.io | bash -s stable --rails #--auto-dotfiles
  dot_file=$(./utils/dot_file.sh)
  echo '
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$HOME/.rvm/bin:$PATH"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" 
# Load RVM into a shell session *as a function*' >>  $dot_file
  source $HOME/.bash_profile
  
  rvm requirements
# ------------------------------------------------------------------------------
log_info "Installing Ruby"
  rvm install 2.3.0
# ------------------------------------------------------------------------------
# You need to run these commands manually
log_info "Creating gemset $GEMSET_NAME to projet"
  rvm use 2.3.0 do rvm gemset create $GEMSET_NAME
# ------------------------------------------------------------------------------
log_info "Defining default ruby version"
  rvm use 2.3.0@$GEMSET_NAME --default
# ------------------------------------------------------------------------------
log_info "Installing bundler"
  gem install bundler
  sudo apt-get install bundler

print_installation_visual_footer