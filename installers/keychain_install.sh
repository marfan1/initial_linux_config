#!/bin/bash

sudo apt-get install keychain
if [ ! -f $HOME/.keychain_init ]; then
  log_info "Creating '$HOME/.keychain_init' file..."

  printf "# After yoou create yours ssh keys, put the name of files in here and uncomment next lines\n" >> $HOME/.keychain_init
  printf "# keychain key_name\n" >> $HOME/.keychain_init
  printf '# . $HOME/.keychain/`uname -n`-sh*' >> $HOME/.keychain_init

  dot_file=$(./utils/dot_file.sh)

  printf "\nsource $HOME/.keychain_init\n" >> $dot_file
  source $HOME/.bash_profile
  log_info "After you create the ssh key, put the name of file in the '$HOME/.keychain_init' file'"

fi
  