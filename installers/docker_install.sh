#!/bin/bash

print_installation_visual_header "DOCKER INSTALLATION"

log_info "Installing docker"
  sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
  sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
  sudo apt-get update
  sudo apt-get install -y docker-engine
  sudo usermod -aG docker $usermod


# ./0_start_config.sh gemsetname(farmio) dbuser(postgres) dbname(farm_io_development) dbpass(admin1234)

  log_info "Running docker postgres container"
  log_info "command: docker run --name postgres -p 5432:5432 -v /var/run/postgresql:/var/run/postgresql -e POSTGRES_USER=$DB_USER -e POSTGRES_DB=$DB_NAME -e POSTGRES_PASSWORD=$DB_PASS -d postgres"

  CONTAINER_ID="$(docker run --name postgres -p 5432:5432 -v /var/run/postgresql:/var/run/postgresql -e POSTGRES_USER=$DB_USER -e POSTGRES_DB=$DB_NAME -e POSTGRES_PASSWORD=$DB_PASS -d postgres)"
  HOST_DB="$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_ID)"

  log_info "CONTAINER_ID: ${CONTAINER_ID}"
  log_info "HOST_DB: ${HOST_DB}"

print_installation_visual_footer

