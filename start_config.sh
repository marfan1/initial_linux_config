#!/bin/bash

sudo chmod +x *.sh
sudo chmod +x  **/*.sh

source ./utils/logger.sh

# chmod +x 0_start_config.sh
# ./start_config.sh gemsetname(farmio) dbuser(postgres) dbname(farm_io_development) dbpass(admin1234)

clear

# gemsetname(farmio) dbuser(postgres) dbname(farm_io_development) dbpass(admin1234)

export GEMSET_NAME="${1-favo}"
export DB_USER="postgres"
export DB_NAME="farm_io_unstable"
export DB_PASS="123fav0!"

function want_to_install() {
  log_info "Do you wish to install $1 [Y/n]? "
  read yn
  case $yn in
      [Yy]* ) ./$2;;
      [Nn]* ) return 0;;
          * ) ./$2;;
  esac
}

# =============================================================================
want_to_install "vcprompt"            installers/vcprompt_install.sh
# =============================================================================
want_to_install "bash_files"          bash_files/install.sh
# =============================================================================
want_to_install "curl"                installers/curl_install.sh
# =============================================================================
want_to_install "rvm"                 installers/rvm_install.sh
# =============================================================================
want_to_install "nginx"               installers/nginx_install.sh
# =============================================================================
want_to_install "nodejs"              installers/nodejs_install.sh
# =============================================================================
want_to_install "gem pg dependencies" installers/pg_dependencies_install.sh
# =============================================================================
want_to_install "sublime text"        installers/sublime_install.sh
# =============================================================================
want_to_install "git"                 installers/git_install.sh
# =============================================================================
want_to_install "git-flow"            installers/git_flow_install.sh
# =============================================================================
want_to_install "docker"              installers/docker_install.sh
# =============================================================================
want_to_install "umake"               installers/umake_install.sh
# =============================================================================
want_to_install "rubymine ide"        installers/rubymine_install.sh
# =============================================================================
want_to_install "android studio"      installers/android_studio_install.sh
# =============================================================================
want_to_install "vim"                 installers/vim_install.sh
# =============================================================================
want_to_install "keychain"            installers/keychain_install.sh

echo "Finished"

