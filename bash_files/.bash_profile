.# Alias definitions.
if [ -f ~/.fan_dot_files/.bash_aliases ]; then
  . ~/.fan_dot_files/.bash_aliases
fi

if [ -f ~/.fan_dot_files/.bash_env_vars ]; then
  . ~/.fan_dot_files/.bash_env_vars
fi

# if running bash
if [ -n "$BASH_VERSION" ]; then
  # include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
  . "$HOME/.bashrc"
  fi
fi

if [ -f ~/.fan_dot_files/.bash_colors ]; then
  . ~/.fan_dot_files/.bash_colors
fi

function function_exists() {
  declare -f -F $1 > /dev/null;
  return $?
}

function apps() {
  directory="$HOME/Documents/apps"
  if [ ! -d $directory ]; then
    printf "Creating directory, wellcome to your new journew of apps development!"
    mkdir $directory
  fi
  cd $directory 
}


function mapps() {
  directory="$HOME/Documents/apps/mobile"
  if [ ! -d $directory ]; then
    printf "Creating directory, wellcome to your new journew of mobile apps development!"
    mkdir $directory
  fi
  cd $directory
}

function wapps() {
  directory="$HOME/Documents/apps/web"
  if [ ! -d $directory ]; then
    printf "Creating directory, wellcome to your new journew of web apps development!"
    mkdir $directory
  fi
  cd $directory
}


function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

function makezip() { zip -r "${1%%/}.zip" "$1" ; }

function extract() {
  if [ -f $1 ] ; then
    case $1 in
        *.tar.bz2)   tar xvjf $1     ;;
        *.tar.gz)    tar xvzf $1     ;;
        *.bz2)       bunzip2 $1      ;;
        *.rar)       unrar x $1      ;;
        *.gz)        gunzip $1       ;;
        *.tar)       tar xvf $1      ;;
        *.tbz2)      tar xvjf $1     ;;
        *.tgz)       tar xvzf $1     ;;
        *.zip)       unzip $1        ;;
        *.Z)         uncompress $1   ;;
        *.7z)        7z x $1         ;;
        *)           printf "'$1' cannot be extracted via >extract<" ;;
    esac
  else
    printf "'$1' is not a valid file!"
  fi
}

function ruby_ver { rvm-prompt; }

function print_before_the_prompt () {
  printf "\n$bldred%s: $bldgrn%s $txtcyn%s$txtrst $txtpur%s\n$txtrst" "$USER" "$PWD" "[$(ruby_ver)]" "$(vcprompt)";

  if [ "$(vcprompt)" = "git:test" ] || [ "$(vcprompt)" = "git:master" ]; then
    printf " $bldred CUIDADO: Você está na branch de $(vcprompt). Não é recomendado fazer alterações direto nesta branch\n$txtrst";
  fi
}

PROMPT_COMMAND=print_before_the_prompt
PS1='$ '



function generate_ssh_key () {

  if (( $# < 1 )) ;  then
    printf "SSH key file name is required\n"
    printf "Usage: generate_ssh_key app_unstable\n"
    printf "This command will generate '~/.ssh/app_unstable' and '~/.ssh/app_unstable.pub files'\n"
    return 1  
  fi
  
  ssh_directory="$HOME/.ssh"

  if [ ! -d $ssh_directory ]; then
    printf "It looks like you do not have the '~/.ssh' directory, would you like to create it now? [Y/n]\n"
    read yn
    if [[ $yn == [Yy]* ]] || [[ -z "$yn" ]] ; then 
      printf "Creating '~/.ssh' directory...\n"
      mkdir $ssh_directory ;
    else
      printf "Sorry, can't continue without the '~/.ssh' directory. \n"
      return 1  
    fi    
  fi
  
  printf "Generating a secure ssh key...\n"

  ssh-keygen -b 4096 -f ~/.ssh/$1 -C $USER@$(hostname) -o -a 500
  printf "Key Generated Successfully! \n"
  printf "Your public key for $ 1 is: \n"
  cat ~/.ssh/$1.pub
  if [ ! -f "$ssh_directory/config" ]; then
    printf "Oh God! Do you still not have the '~/.ssh/config' file to manage your ssh keys? Sorry, I can not ask if you want me to create it automatically, I feel compelled to do it for you.\n"

    sleep 5

    printf "Generating '~/.ssh/config' file...\n"

    generate_git_config $1 > $ssh_directory/config

  fi
}


function generate_git_config () {
  while read -p 'Enter the hostname or ip: ' ssh_hostname && [[ -z "$ssh_hostname" ]] ; do
    printf "\nNo-no, please, no blank hostname! \n" ;
  done
  
  read -p "Enter the host alias: [$1] " ssh_host_alias
  if [[ -z "$ssh_host_alias" ]] ; then ssh_host_alias="$1" ; fi
  
  read -p "Enter the username: [$USER] " ssh_username
  if [[ -z "$ssh_username" ]] ; then ssh_username="$USER" ; fi
  
  printf "Host $1
  HostName  $ssh_hostname
  User $ssh_username
  IdentityFile ~/.ssh/$1"
}