#!/bin/bash

print_installation_visual_header "BASH FILES INSTALLATION"

mkdir $HOME/.fan_dot_files

cp -i -f bash_files/.bash* $HOME/.fan_dot_files/.

if [ -f $HOME/.bash_profile ]; then
  log_info "Sourcing fan_dot_files in your $HOME/.bash_profile"
  printf 'source $HOME/.fan_dot_files/.bash_profile\n' >> $HOME/.bash_profile
elif [[ -f $HOME/.profile ]]; then
  log_info "Sourcing fan_dot_files in your $HOME/.profile"
  printf 'source $HOME/.fan_dot_files/.bash_profile\n' >> $HOME/.profile
fi


print_installation_visual_footer
