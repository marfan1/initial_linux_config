alias ll="ls -laGFH"

# Git aliases 
alias gs="git status"
alias gc="git commit"
alias ga="git add"
alias gd="git diff"
